# WJets analizė naudojant PAT tuple duomenų failus



## PAT tuple failo gamyba

Susikuriame CMSSW aplinką

```bash
cmsrel CMSSW_5_3_32
cd CMSSW_5_3_32/src
cmsenv
```

Atsisiunčiame PAT kodo pavyzdžius ir juos sukompiliuojame:

```bash
git cms-addpkg PhysicsTools/PatAlgos
git cms-addpkg PhysicsTools/PatExamples
scram b
```

Pasigaminkime PAT duomenų failą: tam yra skirtas kodas `PatAlgos` direktorijoje. Jį reikės paredaguoti, nes šiuo metu jis skirtas PAT tuple failų gamybai iš sumodeliuoto duomenų rinkinio, o mes naudojame tikrą.

```bash
cd PhysicsTools/PatAlgos/test
vim patTuple_standard_cfg.py # arba vietoj vim galima naudoti kita redaktoriu
```



Atsidarę failą, jo viršuje pamatome kažką tokio:

```python
## import skeleton process
from PhysicsTools.PatAlgos.patTemplate_cfg import *
```

Po šiuo tekstu pridėkime tokias eilutes: jomis pasakome sistemai, kad detektoriaus „užregistruotų“ objektų nereikia susieti su sumodeliuotais objektais (išlėkusiais iš pagrindinio proceso Feinmano diagramos).

```python
from PhysicsTools.PatAlgos.tools.coreTools import *
removeMCMatching(process, ['All'])
```



Žemiau galime matyti tokias eilutes:

```python
## let it run
process.p = cms.Path(
    process.patDefaultSequence
    )
```

Po jomis pridėkime eilutes, nurodančias *GlobalTag*:

```python
# globaltag
process.GlobalTag.connect = cms.string('sqlite_file:/cvmfs/cms-opendata-conddb.cern.ch/FT_53_LV5_AN1.db')
process.GlobalTag.globaltag = 'FT_53_LV5_AN1::All'
```



Dar žemiau randame eilutes, kuriose nurodomas duomenų rinkinys, jos atrodo taip:

```python
from PhysicsTools.PatAlgos.patInputFiles_cff import filesRelValProdTTbarAODSIM
process.source.fileNames = filesRelValProdTTbarAODSIM
```

Šias eilutes **ištriname** ir vietoje jų įrašome informaciją apie mūsų naudojamus duomenų rinkinius, pavyzdžiui:

```python
# SingleMuon Data

#luminosity
import FWCore.ParameterSet.Config as cms
import FWCore.PythonUtilities.LumiList as LumiList
myLumis = LumiList.LumiList(filename='Cert_190456-208686_8TeV_22Jan2013ReReco_Collisions12_JSON.txt').getCMSSWString().split(',')
process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange()
process.source.lumisToProcess.extend(myLumis)

#input file
import FWCore.Utilities.FileUtils as FileUtils
files2012data = FileUtils.loadListFromFile('CMS_Run2012C_SingleMu_AOD_22Jan2013-v1_20000_file_index.txt')
readFiles = cms.untracked.vstring( * files2012data )
process.source.fileNames = readFiles
```



Daugiau nieko keisti nebereikia, bet iš principo galime pridėti papildomų atrankos kriterijų įvykiams, pavyzdžiui:

```python
process.selectedPatMuons.cut = 'pt > 20. && abs(eta) < 2.4'
```

Juos nurodome pačioje failo apačioje.



Galime pradėti failo gamybą. Tam reikės į tą patį darbinį aplanką (`CMSSW_5_3_32/src/PhysicsTools/PatAlgos/test`) įsikelti reikiamus failus.

Visų pirma, reikalingas duomenų rinkinių sąrašas (pvz., `CMS_Run2012C_SingleMu_AOD_22Jan2013-v1_20000_file_index.txt`) bei duomenų registravimo laikotarpių sąrašas (pvz., `Cert_190456-208686_8TeV_22Jan2013ReReco_Collisions12_JSON.txt`). Šiuos failus galima atsisiųsti iš CMS-OpenData internetinio puslapio.

Taip pat į tą patį aplanką turime pridėti nuorodas į *GlobalTag* failus. Nuorodas sukuriame taip:

```bash
ln -s /cvmfs/cms-opendata-conddb.cern.ch/FT_53_LV5_AN1 FT_53_LV5_AN1
ln -s /cvmfs/cms-opendata-conddb.cern.ch/FT_53_LV5_AN1.db FT_53_LV5_AN1.db
```

Priklausomai nuo naudojamų duomenų failų, *GlobalTag* gali skirtis. Reikiamą versiją taip pat galima pasitikrinti CMS-OpenData puslapyje (būna nurodyta informacijoje prie duomenų rinkinio).

Dabar jau galime paleisti CMSSW modulį:

```bash
cmsRun patTuple_standard_cfg.py
```

Kodas sukurs failą `patTuple_standard.root` su 100 įvykių. Failo pavadinimą ir įvykių skaičių galima pakeisti  konfigūraciniame faile.



## PAT tuple failo analizė

`PatExamples` direktorijoje esančiu kodu galime pasinaudoti failo analizei. Ten jau yra paruoštų „griaučių“ labai paprastoms analizėms. Galime pabandyti paleisti vieną labai paprastą analizę, kuri iš PAT tuple failo atrinks miuonus ir išbrėš jų skersinio impulso, pseudospartos ir azimutinio kampo grafikus. Kodą bus galima redaguoti ir pridėti, kad būtų galima analizuoti daugiau kintamųjų, brėžti daugiau grafikų (arba jų nebrėžti, o saugoti skaičius), bet dabar pabandome tiesiog paleisti esamą pavyzdį.

Savo kątik sukurtą failą perkopijuojame į reikiamą direktoriją:

```bash
cd ../.. # turetume atsidurti direktorijoje CMSSW_5_3_32/src/PhysicsTools
cp PatAlgos/test/patTuple.standard.root PatExamples/test/patTuple.root
```

čia pakeitėme failo pavadinimą iš `patTuple.standard.root` į `patTuple.root`, nes tokį pavadinimą naudoja analizės modulis.

To mums užtenka paleisti analizės moduliui:

```bash
cd PatExamples/test
cmsRun analyzePatMuons_edm_cfg.py
```

Kodas suveiks labai greitai ir sukurs failą `analyzePatMuons.root`, kuriame saugomos trys histogramos.



Norint išgauti daugiau informacijos, kodą reikės redaguoti. Pats `analyzePatMuons_edm_cfg.py` yra tik konfigūracinis failas, todėl jame nėra daug ką redaguoti. 

Iš faile esančių eilučių

```python
process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEDAnalyzer",
  muons = cms.InputTag("cleanPatMuons"),
)
```

galime suprasti, jog analizei naudojamas modulis `PatMuonEDAnalyzer`. 

Šis tipas aprašytas faile `PatExamples/plugins/PatMuonEDAnalyzer.cc`, jį peržiūrėję galime pamatyti, kad jame naudojama klasė `PatMuonAnalyzer`. Ši klasė yra aprašyta dviejuose failuose: `PatExamples/interface/PatMuonAnalyzer.h` („header failas“) ir `PatExamples/src/PatMuonAnalyzer.cc`. Taigi, norėdami pakeisti analizės, vykdomos paleidus `cmsRun analyzePatMuons_edm_cfg.py` komandą, procedūrą, turime redaguoti būtent šiuos du failus.



Pirmiausia galime pridėti eilutes, kurios leis išpiešti grafikus su [trūkstamos skersinės energijos](https://en.wikipedia.org/wiki/Missing_energy) (MET) informacija. Atsidarome failą `PatMuonAnalyzer.h`:

```bash
cd ../interface
vim PatMuonAnalyzer.h # galima naudoti ir bet koki kita teksto redaktoriu
```

Failo apačioje turėtume matyti tokias eilutes:

```c++
private:
  /// input tag for muons
  edm::InputTag muons_;
  /// histograms
  std::map<std::string, TH1*> hists_;
```

Galime matyti, kad miuonų informacija nuo failo bus nuskaitoma pasinaudojant kintamuoju `muons_`. Pridėkime analogišką kintamąjį, kurį naudosime nuskaityti MET informacijai. Dabar ši vieta turėtų atrodyti taip:

```c++
private:
  /// input tag for mouns
  edm::InputTag muons_;
  edm::InputTag METs_;
  /// histograms
  std::map<std::string, TH1*> hists_;
```



Toliau  redaguojame failą `PatMuonAnalyzer.cc`:

```bash
cd ../src
vim PatMuonAnalyzer.cc # galima naudoti ir bet koki kita teksto redaktoriu
```

Pirmiausia turime įtraukti papildomas bibliotekas. Failo viršuje turėtume matyti tokias eilutes:

```c++
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "PhysicsTools/PatExamples/interface/PatMuonAnalyzer.h"
```

Matome, jog yra užkraunami su miuonais susiję duomenų formatai. Pridedame analogiškas eilutes, tik žodžius `Muon` pakeisdami į `MET`. Dabar ši vieta turėtų atrodyti taip:

```c++
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/METReco/interface/MET.h"         // added
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/MET.h"   // added
#include "PhysicsTools/PatExamples/interface/PatMuonAnalyzer.h"
```



Toliau turėtume pakoreguoti klasės konstruktorių. Šiuo metu jis atrodo štai taip:

```c++
/// default constructor
PatMuonAnalyzer::PatMuonAnalyzer(const edm::ParameterSet& cfg, TFileDirectory& fs):
  edm::BasicAnalyzer::BasicAnalyzer(cfg, fs),
  muons_(cfg.getParameter<edm::InputTag>("muons"))
{
  hists_["muonPt"  ] = fs.make<TH1F>("muonPt"  , "pt"  ,  100,  0., 300.);
  hists_["muonEta" ] = fs.make<TH1F>("muonEta" , "eta" ,  100, -3.,   3.);
  hists_["muonPhi" ] = fs.make<TH1F>("muonPhi" , "phi" ,  100, -5.,   5.);
}
```

Galime, matyti, kad jame nurodomas pavadinimas, naudojamas nuskaitant miuonų duomenis (*Input Tag*). Turime pridėti analogišką eilutę, skirtą trūkstamai energijai. Svarbu nepamiršti, po miuonų eilutės padėti kablelį. Taip pat galime pridėti ir MET histogramą. Papildžius ši vieta turėtų atrodyti maždaug taip:

```c++
/// default constructor
PatMuonAnalyzer::PatMuonAnalyzer(const edm::ParameterSet& cfg, TFileDirectory& fs):
  edm::BasicAnalyzer::BasicAnalyzer(cfg, fs),
  muons_(cfg.getParameter<edm::InputTag>("muons")), // added comma
  METs_(cfg.getParameter<edm::InputTag>("METs")) // added
{
  hists_["muonPt"  ] = fs.make<TH1F>("muonPt"  , "pt"  ,  100,  0., 300.);
  hists_["muonEta" ] = fs.make<TH1F>("muonEta" , "eta" ,  100, -3.,   3.);
  hists_["muonPhi" ] = fs.make<TH1F>("muonPhi" , "phi" ,  100, -5.,   5.);
  hists_["METPt"   ] = fs.make<TH1F>("METPt"   , "MET" ,  100,  0., 300.); // added
}
```



Žemiau randame funkcijos `analyze` aprašymą. Joje nurodoma, ką daryti su nuskaitomais įvykiais (analizės algoritmas). Šios funkcijos pradžioje galime pamatyti eilutę, kurioje nurodoma, kuris miuono tipas naudojamas:

```c++
using pat::Muon;
```

Čia šalia nurodome kokį MET tipą naudosime:

```c++
using pat::MET;
```



Žemiau panaudojamas CMSSW įrankis, leidžiantis pasiekti visų įvykyje užregistruotų miuonų informaciją:

```c++
// Handle to the muon collection
edm::Handle<std::vector<Muon> > muons;
event.getByLabel(muons_, muons);
```

Iš karto po to pridedame analogiškas eilutes, leidžiančias pasiekti MET informaciją:

```c++
// Handle to the MET collection
edm::Handle<std::vector<MET> > METs;
event.getByLabel(METs_, METs);
```



Pačioje funkcijos aprašymo apačioje randame `for` ciklą, einantį per visus įvykyje užregistruotus miuonus ir įrašantį kiekvieno miuono informaciją į histogramą:

```c++
// loop muon collection and fill histograms
for(std::vector<Muon>::const_iterator mu1=muons->begin(); mu1!=muons->end(); ++mu1){
  hists_["muonPt" ]->Fill( mu1->pt () );
  hists_["muonEta"]->Fill( mu1->eta() );
  hists_["muonPhi"]->Fill( mu1->phi() );
}
```

Iš karto po šių eilučių galime pridėti `for` ciklą, kuris eis per visus MET elementus (iš tiesų įvykyje visada yra tik vienas MET elementas, nes trūkstamos energijos neįmanoma išskirstyti į dalis, tačiau naudojama tokia duomenų struktūra, kad analizės procedūra nebūtų pernelyg skirtinga nuo visų kitų fizikinių objektų) ir įrašys MET informaciją į histogramą:

```c++
// loop MET collection and fill histograms
for(std::vector<MET>::const_iterator met1=METs->begin(); met1!=METs->end(); ++met1){
  hists_["METPt"]->Fill( met1->pt() );
}
```



Viskas. Dabar galime perkompiliuoti CMSSW grįždami į `src` direktoriją:

```bash
cd ../../../ # we end up in CMSSW_5_3_32/src
scram b
```

Sukompiliavus kodą einame į direktoriją, talpinančią CMSSW konfigūracinius failus:

```bash
cd PhysicsTools/PatExamples/test
```

Tačiau prieš paleisdami `cmsRun` dar turime paredaguoti konfigūracinį failą `analyzePatMuons_edm_cfg.py`. Jį atsidarę randame tokias eilutes:

```python
process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEDAnalyzer",
  muons = cms.InputTag("cleanPatMuons"),
)
```

Galime pastebėti, kad čia nurodoma, į kokią įvesties faile esančią šaką rodo pavadinimas, naudojamas nuskaitant miuonų duomenis (padetalizuojamas *Input Tag*). Miuonų atveju ta šaka yra `"cleanPatMuons"`. Turime nurodyti analogišką informaciją, atitinkančią MET šaką. Galime patikrinti `patTuple.root` failą ir pamatyti, kad jame MET informacija saugoma šakoje `patMETs`. Prirašome šią informaciją. Tai padarius, viskas turėtų atrodyti taip:

```python
process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEDAnalyzer",
  muons = cms.InputTag("cleanPatMuons"), METs = cms.InputTag("patMETs"),
)
```



Dabar jau galime paleisti analizės kodą:

```bash
cmsRun analyzePatMuons_edm_cfg.py
```

Jeigu norėtume išanalizuoti daugiau įvykių informacijos (pvz., įtraukti informaciją apie elektronus, čiurkšles), kodą turėtume išplėsti labai panašiai, kaip ir MET atveju. Konkrečiau reikiamas pridėti klases galima pasitikrinti [CMS Twiki puslapyje](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookPAT) bei [CMSSW kode](https://github.com/cms-sw/cmssw/tree/CMSSW_5_3_X). Svarbu nepamiršti, kad pakoregavus `C++` kodą viską reikia perkompiliuoti, t.y., grįžti į `src` direktoriją ir paleisti komandą `scram b`.



## ROOT medžio sukūrimas analizuojant PAT tuple failą

Naudotą analizės kodą galima „privesti“ ne tik piešti grafikus, bet ir kurti `.root` failus, kuriuose bus saugoma konkreti informacija apie kiekvieną įvykį, o šių failų struktūrą galime susikurti kokią norime. Įprastai, kai norime išsaugoti konkrečią informaciją apie kiekvieną įvykį ir vėliau šią informaciją pasiimti kuriam norime įvykiui (o ne tiesiog žiūrėti į bendrą statistiką, kaip histogramų atveju), naudojame ROOT medžius. Apie juos plačiau galima paskaityti [čia](https://root.cern.ch/doc/master/classTTree.html), o [čia](https://root.cern/manual/trees/) galima atlikti paprastas pavyzdines užduotis, kad būtų galima suprasti, kaip ROOT medžiai veikia, kokia jų struktūra ir pan.

Norint informaciją saugoti ROOT medžiuose mums vėl reikės redaguoti failus `PatMuonAnalyzer.h` and `PatMuonAnalyzer.cc`. Pirmiausia atsidarome `PatMuonAnalyzer.h`. Jame pirmiausia turime pridėti mums reikiamas bibliotekas:

```c++
#include "TFile.h"
#include "TTree.h"
```

Toliau galime pamatyti, kad `PatMuonAnalyzer` klasė turi tris funkcijas: `beginJob`, `Analyze` ir `endJob`. Funkcijos `beginJob` ir `endJob` šiuo metu yra tuščios, o funkcija `Analyze` yra aprašyta faile `PatMuonAnalyzer.cc`. Ją mes redagavome ankstesniame užduoties etape.

Iš principo, funkcijoje `beginJob` turėtume padaryti visus paruošiamuosius žingsnius, kuriuos reikia atlikti prieš pradedant įvykių analizę. Funkcija `Analyze` atlieka visus joje aprašytus žingsnius kiekvienam įvykiui. Funkcija `endJob` atlieka baigiamuosius žingsnius, kuriuos būtina atlikti pabaigus įvykių analizę. Taigi, `beginJob` mums tinka sukurti ROOT medžiui, funkcijoje `Analyze` galėsime informaciją supildyti į ROOT medį, o funkcijoje `endJob` galime sukurtą ROOT medį išsaugoti faile. 

Pirmiausia pradėkime nuo privačių kintamųjų pridėjimo (jie yra `PatMuonAnalyzer` klasės aprašymo apačioje). Šiuo metu galime matyti tokius privačius kintamuosius:

```c++
private:
  /// input tag for mouns
  edm::InputTag muons_;
  edm::InputTag METs_;
  /// histograms
  std::map<std::string, TH1*> hists_;
```

Čia galime pridėti kintamuosius, kuriuose laikysime kiekvieno įvykio informaciją, ROOT medį, kuriame tą informaciją saugosime bei patį išvesties `.root` failą. Tai turėtų atrodyti maždaug taip:

```c++
private:
  /// input tag for mouns
  edm::InputTag muons_;
  edm::InputTag METs_;
  /// histograms
  std::map<std::string, TH1*> hists_;

  // Output file and tree
  TFile *f_out;
  TTree *t_out;
  // Custom defined event variables
  std::vector<double> *muon_pT;
  double MET_pT;
```



Dabar jau galime redaguoti naudojamas funkcijas. Pirmiausia papildome funkciją `beginJob`. Joje sukuriame ROOT medį ir išvesties `.root` failą. Taip pat sukuriame medžio struktūrą:

```c++
/// everything that needs to be done before the event loop
void beginJob(){
  f_out = new TFile("OUTFILE.root", "RECREATE"); // Creating the file OUTFILE.root
  f_out->cd();
  t_out = new TTree("TREE", "TREE"); // Creating a tree with name TREE
  t_out->Branch("muon_pT", &muon_pT); // One branch of the tree will be muon_pT
  t_out->Branch("MET_pT", &MET_pT); // Another branch will be MET_pT
};
```

Tada papildome funkciją `endJob` eilutėmis, kurios įrašys ROOT medį į sukurtą failą:

```c++
/// everything that needs to be done after the event loop
void endJob(){
  f_out->cd(); // to make sure we are writing into this file
  t_out->Write();
  f_out->Close();
};
```



Galiausiai galime papildyti funkciją `Analyze`. Ji, kaip žinia, yra aprašyta faile `PatMuonAnalyzer.cc`, tad turime uždaryti failą `PatMuonAnalyzer.h` ir atsidaryti `PatMuonAnalyzer.cc` bei susirasti, kur prasideda funkcijos `Analyze` aprašymas

Pirmiausia pačiame funkcijos aprašymo pradžioje pridėkime eilutes, kurios užtikrina, kad vektorius, kuriame saugosime miuonų skersinio impulso informaciją, yra tuščias (kitaip tariant, įsitikiname, kad išsaugosime tik esamo įvykio informaciją):

```c++
// clear vectors
muon_pT->clear();
```

Žemiau susirandame `for` ciklą, einantį per miuonus. Jo viduje pridedame tokias eilutes:

```c++
// writing muon info into a vector
muon_pT->push_back( mu1->pt() );
```

Taip į vektorių `muon_pT` pridėsime elementą, atitinkantį vieno iš miuonų skersinį impulsą, o for ciklas užtikrins, kad įrašome kiekvieno miuono informaciją.

Dar žemiau susirandame `for` ciklą, einantį per MET objektus (jų yra tik vienas, bet ciklas vis tiek rašomas). Jo viduje pridedame tokias eilutes:

```c++
// writing MET information into a variable
MET_pT = met1->pt();
```

Čia nenaudojame vektoriaus, nes MET turi tik vieną elementą.

Galiausiai įrašome įvykio informaciją į ROOT medį. Pačioje funkcijos apačioje pridedame tokias eilutes:

```c++
// Saving event info into a tree
t_out->Fill();
```

Viskas. Dabar užtenka grįžti į `CMSSW_5_3_32/src` direktoriją ir viską sukompiliuoti su komanda `scram b`. Dabar paleidus komandą `cmsRun analyzePatMuons_edm_cfg.py` šalia failo `analyzePatMuons.root` su grafikais kartu bus sukuriamas ir failas `OUTFILE.root` su ROOT medžiu. Jį galima apžiūrėti naudojants `TBrowser`.

Vėliau į failą reikėtų pridėti daugiau su įvykiais susijusios informacijos.

